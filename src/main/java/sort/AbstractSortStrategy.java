package sort;

public abstract class AbstractSortStrategy {
    protected Integer[] array;

    public AbstractSortStrategy() {
    }

    public void setArray(Integer[] array) {
        this.array = array;
    }

    public abstract void sort();

    /**
     * Wypisuje tablicę.
     */
    public void print() {
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i] + ", ");
        }
    }

    /**
     * Wykonuje sortowanie, ale mierzy czas sortowania i wypisuje go na ekran.
     */
    public void sortAndMeasureTime() {
        // czas rozpoczęcia pomiaru
        Long nanoStart = System.nanoTime();

        // sortowanie
        sort();

        // czas zakończenia pomiaru
        Long nanoStop = System.nanoTime();

        // wypisanie wyniku czasu sortowania
        System.out.println("Czas sortowania: " + (nanoStop - nanoStart));
    }

}
