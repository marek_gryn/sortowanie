package sort.strategies;

import sort.AbstractSortStrategy;

public class BubbleSort extends AbstractSortStrategy {

    public BubbleSort() {
        super();
    }

    public void sort() {
        int n=array.length;
        int tmp;
        do{
            for (int i = 0; i <n-1; i++) {
                if(array[i]>array[i+1]){
                    tmp=array[i+1];
                    array[i+1]=array[i];
                    array[i]=tmp;
                }
            }
            n--;
        }while (n>1);
    }
}
