package sort.strategies;

import sort.AbstractSortStrategy;

public class MergeSort extends AbstractSortStrategy {
    public MergeSort() {
        super();
    }

    public void sort() {
        mergeSort(array,0,array.length-1);
    }

    public void mergeSort(Integer[] tablica, int first, int last) {
        if (first != last) {
            //split lewej strony
            int middle = ((last - first) / 2) + first;
            mergeSort(tablica, first, middle);
            //split prawej strony
            mergeSort(tablica, middle + 1, last);

            //laczenie (merge) obu stron
            marge(tablica, first, middle, last);
        }
    }

    public void marge(Integer[] tablica, int first, int middle, int last) {
        Integer[] tmp = new Integer[tablica.length];
        for (int i = 0; i < tablica.length; i++) {
            tmp[i] = tablica[i];
        }

        int i1 = first;
        int i2 = middle + 1;
        int currentIndex = first;

        while ((i1 <= middle) && (i2 <= last)) {
            if (tmp[i1] > tmp[i2]) {
                tablica[currentIndex] = tmp[i2];
                i2++;
            } else {
                tablica[currentIndex] = tmp[i1];
                i1++;
            }
            currentIndex++;
        }

        for (int i = i1; i <= middle; i++) {
            tablica[currentIndex++] = tmp[i];
        }
        for (int i = i2; i <= last; i++) {
            tablica[currentIndex++] = tmp[i];
        }
    }
}
