package sort.strategies;

import sort.AbstractSortStrategy;

public class QuickSort extends AbstractSortStrategy {

    public QuickSort() {
        super();
    }

    public void sort() {
        quickSort(array, 0, (array.length - 1));
    }

    public void quickSort(Integer[] tablica, int left, int right) {
        if (left != right) {
            int pivot = partition(tablica, left, right);
            quickSort(tablica, left, pivot - 1);
            quickSort(tablica, pivot + 1, right);
        }
    }

    public int partition(Integer[] tablica, int left, int right) {
        int x = tablica[left];
        int indexLeft = left - 1;
        int indexRight = right + 1;
        while (true) {
            while (true) {
                indexLeft++;
                if (tablica[indexLeft] >= x) {
                    break;
                }
            }
            while (true) {
                indexRight--;
                if (tablica[indexRight] <= x) {
                    break;
                }
            }
            if (indexLeft < indexRight) {
                int tmp = tablica[indexLeft];
                tablica[indexLeft] = tablica[indexRight];
                tablica[indexRight] = tmp;
            } else {
                return indexRight;
            }
        }
    }
}
